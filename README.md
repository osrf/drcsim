# REPOSITORY MOVED

## This repository has moved to

https://github.com/osrf/drcsim

## Issues and pull requests are backed up at

https://osrf-migration.github.io/drcsim-gh-pages/#!/osrf/drcsim

## Until BitBucket removes Mercurial support, this read-only mercurial repository will be at

https://bitbucket.org/osrf-migrated/drcsim

## More info at

https://community.gazebosim.org/t/important-gazebo-and-ignition-are-going-to-github/533

